 #include <pthread.h> // notice pthread function
 #include <stdio.h>
 #include <time.h>
 #include <sys/time.h>
 
 #define NUM_THREADS 8
 
 #define ROWS_A 	3000
 #define COLUMNS_A 	3000
 #define ROWS_B 	3000
 #define COLUMNS_B 	3000
 
int matA[ROWS_A][COLUMNS_A]; 
int matB[ROWS_B][COLUMNS_B]; 
int matC[ROWS_A][COLUMNS_B]; 

 void *multiply(void *threadid)
 {
    long tid;
    tid = (long)threadid;
    
    //
    // A range of operation is calculated for each thread
	// Notice that, for simplicity, ROWS_A is divisible by NUM_THREADS, for a general case
	// this calculation would be more complex
    //
    int thread_range_init_A = tid*(ROWS_A / NUM_THREADS);
    int thread_range_end_A = tid*(ROWS_A / NUM_THREADS) + (ROWS_A / NUM_THREADS)-1;
    printf("thread %ld does from %d to %d \n", tid, thread_range_init_A, thread_range_end_A);
   
    //
    // Thread individual matrix multiplication
    //
    for (int c = thread_range_init_A ; c <= thread_range_end_A ; c++ )
    {
      for (int d = 0 ; d < COLUMNS_B ; d++ )
      {
        for (int k = 0 ; k < COLUMNS_A ; k++ ) 
        {
          matC[c][d] += matA[c][k]*matB[k][d];
        }
      }
    }  
 }

 int main (int argc, char *argv[])
 {
 	clock_t start, end;
 	double total_time;
    pthread_t threads[NUM_THREADS];
    int rc;
    long t;
    
    if ( COLUMNS_A != ROWS_B ) {
    	printf("Matrix sizes dont match\n");
    	return 1;
    }
    
    //
    // Generating random values in matA and matB 
    //
    for (int i = 0; i < ROWS_A; i++) { 
        for (int j = 0; j < COLUMNS_A; j++) { 
            matA[i][j] = i+1; 
        } 
    } 
    for (int i = 0; i < ROWS_B; i++) { 
        for (int j = 0; j < COLUMNS_B; j++) { 
            matB[i][j] = j+1; 
        } 
    } 
    for (int i = 0; i < ROWS_B; i++) { 
        for (int j = 0; j < COLUMNS_B; j++) { 
            matC[i][j] = 0; 
        } 
    } 
    
    //
    // Here are threads created and deployed to process the "dot" function,
    // passed as an argument
    //
    printf("deploying threads\n");
    struct timeval timer_start, timer_end;
	gettimeofday(&timer_start, NULL);
    for(t = 0; t < NUM_THREADS; t++) {
       rc = pthread_create(&threads[t], NULL, multiply, (void *)t);
       if (rc) {
          printf("ERROR; return code from pthread_create() is %d\n", rc);
          return 1;
       }
    }    
    for(t = 0; t < NUM_THREADS; t++)
    {
		pthread_join(threads[t], NULL);
	}
	gettimeofday(&timer_end, NULL);
	
	double time_spent = timer_end.tv_sec - timer_start.tv_sec + (timer_end.tv_usec - timer_start.tv_usec) / 1000000.0;
	printf("Time spent: %.6f \n", time_spent);
	
	// Displaying the result matrix 
   /* printf("Multiplication of A and B\n"); 
    for (int i = 0; i < ROWS_A; i++) { 
        for (int j = 0; j < COLUMNS_B; j++)  
            printf("%d ", matC[i][j]);         
        printf("\n");
    } */
    
    pthread_exit(NULL);
    
    return 0;
 }

