 #include <pthread.h>
 #include <stdio.h>
 #define NUM_THREADS 4

 void *PrintHello(void *threadid)
 {
    long tid;
    tid = (long)threadid;
    printf("Hello World! from thread #%ld!\n", tid);
 }

 int main (int argc, char *argv[])
 {
    pthread_t threads[NUM_THREADS];
    int rc;
    long t;
    for(t = 0; t < NUM_THREADS; t++) {
       rc = pthread_create(&threads[t], NULL, PrintHello, (void *)t);
       if (rc) {
          printf("ERROR; return code from pthread_create() is %d\n", rc);
          return 1;
       }
    }

    /* Last thing that main() should do */
    pthread_exit(NULL);
    
    return 0;
 }

