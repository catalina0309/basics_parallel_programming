#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h> // notice omp function
#include <sys/time.h>

#define NUM_THREADS 8
 
#define ROWS_A 8
#define COLUMNS_A 8
#define ROWS_B 8
#define COLUMNS_B 8

int matA[ROWS_A][COLUMNS_A]; 
int matB[ROWS_B][COLUMNS_B]; 
int matC[ROWS_A][COLUMNS_B]; 

int main() 
{
    int i,j,k;
    struct timeval tv1, tv2;
    struct timezone tz;
	double elapsed; 
	
	//
	// if I want to use all the resources, I may use omp_get_num_procs() function
	//
    omp_set_num_threads(NUM_THREADS);
    
    //
    // Generating random values in matA and matB 
    //
    for (int i = 0; i < ROWS_A; i++) { 
        for (int j = 0; j < COLUMNS_A; j++) { 
            matA[i][j] = i+1; 
        } 
    } 
    for (int i = 0; i < ROWS_B; i++) { 
        for (int j = 0; j < COLUMNS_B; j++) { 
            matB[i][j] = j+1; 
        } 
    } 
    for (int i = 0; i < ROWS_B; i++) { 
        for (int j = 0; j < COLUMNS_B; j++) { 
            matC[i][j] = 0; 
        } 
    } 
	
	//
	// actual computation starts
	//
    gettimeofday(&tv1, &tz);
    
    #pragma omp parallel for private(i,j,k) shared(matA,matB,matC)
    for (i = 0; i < ROWS_A; ++i) {
        for (j = 0; j < COLUMNS_B; ++j) {
            for (k = 0; k < COLUMNS_A; ++k) {
                matC[i][j] += matA[i][k] * matB[k][j];
            }
        }
    }

    //
	// present total execution time
	//
    gettimeofday(&tv2, &tz);
    elapsed = (double) (tv2.tv_sec-tv1.tv_sec) + (double) (tv2.tv_usec-tv1.tv_usec) * 1.e-6;
    printf("elapsed time = %f seconds.\n", elapsed);
    
    // Displaying the result matrix 
    /*printf("Multiplication of A and B\n"); 
    for (int i = 0; i < ROWS_A; i++) { 
        for (int j = 0; j < COLUMNS_B; j++)  
            printf("%d ", matC[i][j]);         
        printf("\n");
    } */
	
	return 0;
}
