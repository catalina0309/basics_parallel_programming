 #include <pthread.h> // notice pthread function
 #include <stdio.h>
 #include <time.h>
 #include <sys/time.h>
 
 #define NUM_THREADS 2
 #define VECTOR_SIZE 100000000

int VectorA[VECTOR_SIZE]; 
int VectorB[VECTOR_SIZE];

//
long long dot_product = 0;
int mySum[NUM_THREADS];

pthread_mutex_t mtxsum;

 void *dot(void *threadid)
 {
    long tid = (long)threadid;
    
    //
    // A range of operation is calculated for each thread
		// Notice that, for simplicity, VECTOR_SIZE is divisible by NUM_THREADS, for a general case
		// this calculation would be more complex
    //
    int thread_range_start = tid*(VECTOR_SIZE / NUM_THREADS);
    int thread_range_end = tid*(VECTOR_SIZE / NUM_THREADS) + (VECTOR_SIZE / NUM_THREADS)-1;
    
    //
    // Per thread computation starts, storing individual values in mySum[tid]
    //
    for (int c = thread_range_start ; c <= thread_range_end ; c++ )
    {
    	mySum[tid] += VectorA[c] * VectorB[c];
    }
    
    //
    // Becase variable dot_product is global, and all threads may want to 
    // access it at the same time, causing possibly incorrect results,
    // a lock is acquired and released
    //
    pthread_mutex_lock(&mtxsum);
    dot_product += mySum[tid];
    pthread_mutex_unlock(&mtxsum);
    
 }

 int main (int argc, char *argv[])
 {
 	struct timeval timer_start, timer_end;
 	double time_spent;
  pthread_t threads[NUM_THREADS];
  int rc;
  
  //
  // Filling vectors with whatevr numbers
  //
	for (int i = 0; i < VECTOR_SIZE ; i++)
	{
		VectorA[i] = i;
		VectorB[i] = i+1;
	}
	
  //
  // Here are threads created and deployed to process the "dot" function,
  // passed as an argument
  // 
  printf("deploying threads\n");
    
	gettimeofday(&timer_start, NULL);
    for(long t = 0; t < NUM_THREADS; t++) {
       rc = pthread_create(&threads[t], NULL, dot, (void *)t);
       if (rc) {
          printf("ERROR; return code from pthread_create() is %d\n", rc);
          return 1;
       }
    }    
  for(int t = 0; t < NUM_THREADS; t++)
		pthread_join(threads[t], NULL);
		
	gettimeofday(&timer_end, NULL);
	
	//
	// present total execution time
	//
	time_spent = timer_end.tv_sec - timer_start.tv_sec + (timer_end.tv_usec - timer_start.tv_usec) / 1000000.0;
	printf("Time spent: %.6f \n", time_spent);
	
	//printf("dot product: %lld\n",dot_product);
	
	pthread_mutex_destroy(&mtxsum);
  pthread_exit(NULL);
    
  return 0;
  
 }

